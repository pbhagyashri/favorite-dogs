import Axios from 'axios'

export const fetchBreeds = () => async dispatch => {
  const breeds = await Axios.get('https://dog.ceo/api/breeds/list/all')
  dispatch({ type: 'FETCH_BREEDS', payload: breeds.data })
}

export const searchBreed = breedName => async dispatch => {
  const breed = await Axios.get(
    `https://dog.ceo/api/breed/${breedName}/images/random`
  )
  dispatch({
    type: 'SEARCH_BREED',
    payload: { image: breed.data.message, name: breedName }
  })
}

export const addRandomBreed = () => async dispatch => {
  const randomBreed = await Axios.get(`https://dog.ceo/api/breeds/image/random`)
  const breed = {
    name: randomBreed.data.message
      .split('breeds/')
      .pop()
      .split('/')[0],
    image: randomBreed.data.message
  }
  dispatch({ type: 'RANDOM_BREED', payload: breed })
}
