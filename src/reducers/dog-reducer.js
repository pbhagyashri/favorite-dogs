const initialState = {
  allBreeds: {},
  searchedBreeds: [],
  allSearchedBreeds: []
}
export default function(state = initialState, action) {
  switch (action.type) {
    case 'FETCH_BREEDS':
      return {
        ...state,
        allBreeds: { ...action.payload.message }
      }
    case 'SEARCH_BREED':
      return {
        ...state,
        searchedBreeds: [action.payload, ...state.searchedBreeds]
      }
    case 'RANDOM_BREED':
      return {
        ...state,
        searchedBreeds: [...state.searchedBreeds, action.payload]
      }

    default:
      return state
  }
}
