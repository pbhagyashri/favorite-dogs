import React, { Component } from 'react'
import Header from './components/header/Header'
import SearchBreeds from './components/search-breeds/SearchBreeds'
import Breeds from './components/breeds/Breeds'
import { connect } from 'react-redux'
import * as actions from './actions'
import { BrowserRouter, Route } from 'react-router-dom'

class App extends Component {
  //laod all breed when app starts and App component mounts in the DOM
  //Save all bredds to Redux store
  componentDidMount() {
    this.props.fetchBreeds()
  }
  render() {
    return (
      <div>
        <BrowserRouter>
          <Header />
          <Route path='/breeds' component={Breeds}></Route>
          <Route exact path='/' component={SearchBreeds}></Route>
        </BrowserRouter>
      </div>
    )
  }
}

export default connect(null, actions)(App)
