import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import RenderCollection from './RenderCollection'
import './SearchBreeds.css'
import Autocomplete from 'react-autocomplete'

class SearchBreeds extends Component {
  constructor() {
    super()
    this.state = {
      breedName: '',
      allSearches: []
    }
  }

  handleInput = e => {
    e.preventDefault()
    this.setState({
      breedName: e.target.value.toLowerCase()
    })
  }

  handleSubmit = val => {
    if (!this.state.allSearches.includes(val)) {
      this.state.allSearches.push(val)
      this.props.searchBreed(val)
    } else {
      alert(
        'Breed aleardy exist in the collection. Please search for another breed'
      )
    }
  }

  keyPress = e => {
    if (e.keyCode === 13) {
      e.preventDefault()
      let currentSearch = this.state.breedName
      if (!this.state.allSearches.includes(currentSearch)) {
        this.state.allSearches.push(currentSearch)
        this.props.searchBreed(currentSearch)
      } else {
        alert(
          'Breed aleardy exist in the collection. Please search for another breed'
        )
      }
    }
  }

  render() {
    const { breedName } = this.state
    return (
      <div className="collection-grid">
        <h1 className="heading">Search for your favorite dog images!</h1>
        <section className="search-bar">
          {/* form and label are only for seo and accessiblity */}
          <form>
            <i className="fa fa-search searchIcon"></i>
            <Autocomplete
              className="autocomplete-input"
              getItemValue={item => item.label}
              items={Object.keys(
                this.props.breed.favoriteDogs.allBreeds
              ).map(name => ({ label: name }))}
              renderItem={(item, isHighlighted) => (
                <div
                  className="autocomplete-input-panel"
                  style={{ background: isHighlighted ? 'lightgray' : 'white' }}
                >
                  {item.label}
                </div>
              )}
              value={breedName}
              key={breedName}
              onChange={e => this.handleInput(e)}
              onSelect={val => this.handleSubmit(val)}
            />
          </form>
        </section>

        <RenderCollection />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    breed: state
  }
}

export default connect(mapStateToProps, actions)(SearchBreeds)
