import React, { Component } from 'react'
import { connect } from 'react-redux'
import './SearchBreeds.css'
import SearchRandomBreed from '../random-breed/SearchRandomBreed'
import * as actions from '../../actions'

class RenderCollection extends Component {
  displayBreeds(breeds) {
    return breeds.map((breed, index) => (
      <div className="image-card" key={index}>
        <img src={breed.image} alt="dog" className="card-images" />
        <span className="breed-name">{breed.name}</span>
      </div>
    ))
  }

  render() {
    return (
      <div className="cards">
        {!this.props.breed
          ? 'loading'
          : this.displayBreeds(this.props.breed.favoriteDogs.searchedBreeds)}
        <SearchRandomBreed />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    breed: state
  }
}

export default connect(mapStateToProps, actions)(RenderCollection)
