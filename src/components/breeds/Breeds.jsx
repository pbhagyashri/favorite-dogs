import React, { Component } from 'react'
import { connect } from 'react-redux'

class Breeds extends Component {
  render() {
    return (
      <main>
        <section>
          <ul>
          {Object.keys(this.props.breeds.favoriteDogs.allBreeds).map((keyName, keyIndex) => ( <li>{keyName}</li>))}
          </ul>
        </section>
      </main>
    )
  }
}

const mapStateToProps = state => {
  return {
    breeds: state
  }
}

export default connect(mapStateToProps, null)(Breeds)
