import React, { Component } from 'react'
import './SearchRandomBreed.css'
import * as actions from '../../actions'
import { connect } from 'react-redux'

class SearchRandomBreed extends Component {
  handleClick = async () => {
    this.props.addRandomBreed()
  }

  render() {
    return (
      <div className="image-card random-breed-card">
        <button onClick={this.handleClick} className="roundButton">
          +
        </button>
      </div>
    )
  }
}

export default connect(null, actions)(SearchRandomBreed)
