import React from 'react'
import './Header.css'
const Header = () => {
  return (
    <header>
      <nav>
        <div className="logo-cont">
          <a href="/" className="logo">
            <i className="fa fa-paw fa-2x" aria-hidden="true"></i>
          </a>
          <p className="app-name">Woof</p>
        </div>
        <a href="/breeds" className="nav-links">
          ALL BREADS
        </a>
      </nav>
    </header>
  )
}

export default Header
